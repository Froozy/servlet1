package com.example.servletjspdemo.domain;

import java.util.List;



public class PersonList {
	public List<Person> list;

	public Person getfromList(int id) {
		return this.list.get(id);
	}

	public void addtoList(Person person) {
		this.list.add(person);
	}
	
}
