package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class FirstServlet
 */
@WebServlet("/FirstServlet")
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		response.setContentType("text/html");
		response.getWriter().println("<h1>hello "+name+"</h1>");
		HttpSession session = request.getSession();
		ServletContext ctx = request.getServletContext();
		if(name!=null&&!name.equalsIgnoreCase(""))
			{
				session.setAttribute("name", name);
				ctx.setAttribute("name", name);
			}

		response.getWriter().println("<h1>hello from session "+session.getAttribute("name")+"</h1>");
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"First name: <input type='text' name='firstName' /> <br />" +
				"Second name: <input type='text' name='secondName' /> <br />" +
				"<input type='radio' name='hobby' value='praca'>Ogloszenie w pracy<br />" +
				"<input type='radio' name='hobby' value='uczelnia'>Ogloszenie na uczelni<br />" +
				"<input type='radio' name='hobby' value='fb'>Facebook<br />" +
				"<input type='radio' name='hobby' value='friends'>Znajomi<br />" +
				"<input type='radio' name='hobby' value='other'>Inne<br />" +"<input type = 'text' name = Innetext>"+
				"Czym si� zajmujesz?<textarea name='work' cols='30' rows='15'>Opisz sw�j zaw�d</textarea>"+
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
